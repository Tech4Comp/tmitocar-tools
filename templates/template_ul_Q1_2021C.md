# Dein Feedback zur Schreibaufgabe -subject-
Dieses Dokument enthält eine Analyse zu deiner Schreibaufgabe zum Thema -subject-. Die Analyse wurde automatisch auf der Grundlage der von dir bearbeiteten Schreibaufgabe erstellt. Dabei wurden die Wissensstrukturen, die dein Text enthältgraphisch dargestellt, mit den Seminartexten verglichen und für dich interpretiert. Das Ergebnis dieser Analyse steht dir hier zur Verfügung. Wir vom tech4comp-Team wünschen dir viel Spaß und Erfolg mit dieser Analyse!

## 1. Wissensmodell
In diesem Abschnitt wird das Wissensmodell aus deinem Text präsentiert und mit dem Modell des Seminartextes (sog. Expertentext) verglichen. Die folgende Grafik zeigt das Modell zu deiner Ausarbeitung zum Thema -subject-:

| -graph2- |
|:--:|
| *Abbildung (1): Dein Modell zum Thema -subject-* |

Mittels einer computerlinguistischen Analysesoftware wurde die oben stehende Grafik auf der Grundlage deines Textes erstellt. Sie enthält die wichtigsten in deinem Text vorkommenden Ausführungen zum Thema -subject- und zeigt, wie sie miteinander verknüpft sind. In deinem Text besonders stark assoziierte Begriffsverbindungen werden in roter Farbe dargestellt. Du erkennst dies auch daran, dass die Assoziationsstärke zwischen den Begriffen besonders hoch ist (das ist die kleine Zahl außerhalb der Klammer, und sie liegt zwischen 0 und 1). Darüber hinaus wichtige, aber weniger stark assoziierte Begriffsverbindungen werden in blau dargestellt.
Achte bei der Betrachtung auch auf das gesamte Netzwerk: Welche Begriffe sind zentral und gehen viele Verbindungen mit benachbarten Konzepten ein? Welche stehen eher am Rand und sind kaum mit dem Modell verbunden?

Als Vergleich zu deinem Text zeigen wir dir anschließend das sogenannte ‚Expertenmodell‘, eine Darstellung des Seminartextes, auf den sich deine Schreibaufgabe bezieht:

| -graph1- |
|:--:|
| *Abbildung (2): Das Expertenmodell zum Thema -subject-* |

Dieses ‘Expertenmodell’ basiert auf dem Seminartext und enthält die wichtigsten Vorstellungen zum Thema -subject-. Du kannst es gut zur Wiederholung und Überprüfung deines Wissensstandes nutzen: Schau dir die zentralen Begriffe an und überlege, ob du die Verbindungen, die davon ausgehen, nachvollziehen und erläutern kannst. Vielleicht fehlen auch Begriffe, die du selbst wichtig findest, die aber hier nicht aufgenommen werden - das kann daran liegen, dass sie im Text nur kurz erwähnt werden und daher für dieses Modell nicht berücksichtigt wurden.

## 2. Vergleich der Modelle

In diesem Abschnitt wird deine Schreibaufgabe zum Thema -subject- mit dem entsprechenden Seminartext verglichen. Natürlich kannst du das auch selbst tun, indem du die beiden vorher abgebildeten Netze betrachtest und nach Gemeinsamkeiten und Unterschieden suchst. Als Hilfestellung für diesen Vergleich bieten wir in diesem Abschnitt zwei Wortlisten an.

Die erste Liste zeigt die Begriffe, die sowohl in deinem Text als auch im Expertentext vorkommen. Damit kannst du auf einen Blick die ‚Schnittmenge' dieser beiden Texte sehen - zumindest was die Begriffe angeht. Gibt es darüber hinaus Begriffe, die dir für die Aufgabe wichtig erscheinen, die im Expertentext vielleicht sogar in der Nähe der Schnittmenge liegen, in deinem Text aber anders oder weniger aufgeführt werden?

| -BegriffeSchnittmenge- |
|:--:|
| *Abbildung 3: Liste der gemeinsamen Konzepte* |

Die zweite Liste sammelt die Begriffe, die zwar der Expertentext verwendet hat, jedoch nicht in deiner Schreibaufgabe vorkommen. Mit Hilfe dieser Liste kannst du gut sehen, welche Aspekte des Themas in deinem Text weniger stark behandelt werden. Das kann entweder an dem Schwerpunkt liegen, den du gewählt hast, oder daran, dass im Expertentext weiterführende Inhalte angeschnitten werden, die über die konkrete Schreibaufgabe hinausgehen. Gleichzeitig kannst du versuchen, diese Wortliste als Anregung zu nutzen – um zu überlegen, ob sich manche Begriffe eignen könnten, deinen eigenen Text zu erweitern oder zu präzisieren.


| -BegriffeDiffA- |
|:--:|
| *Abbildung 4: Liste von Konzepten, die nur im Expertentext vorkommen* |

## 3. Abschließende Bemerkung

Bei dem gesamten Analysevorgang zur Erstellung dieses Dokuments wurden keine personenbezogenen Daten von dir gespeichert. Auch deine Texte werden von den Systemen an der Martin-Luther-Universität Halle-Wittenberg wieder vollständig gelöscht.
Diese Auswertung ist Teil des vom BMBF geförderten Verbundprojektes tech4comp. Die Software für diese Auswertung (T-MITOCAR) wurde von Prof. Dr. Pablo Pirnay-Dummer für das Projekt entwickelt und steht Projektpartnern und kooperierenden Projekten zur Analyse zur Verfügung. Die Software hatte zum Zeitpunkt dieser Auswertung die Version 0.0.3.
Das Entwicklungsteam der Software hat möglicherweise keinen Einfluss darauf, in welchem didaktischen Rahmen diese Software eingesetzt wird. Zur Anwendung der Software in deiner Lehrveranstaltung frage bitte deine Dozentinnen und Dozenten.
