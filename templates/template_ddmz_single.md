# Dein Feedback (mit TASA / T-TMITOCAR)

## Textanalyse (themenunabhängig)

Dieses Dokument enthält eine Analyse zu dem von dir hochgeladenen Text. Die Analyse wurde für dich automatisch und ausschließlich auf der Grundlage deines Textes erstellt. Dabei wurden die Wissensstrukturen, die in deinem Text verfasst sind, mittels der Software T-MITOCAR computer-linguistisch extrahiert und grafisch dargestellt. Das Ergebnis dieser Analyse steht dir hier zur Verfügung. Wir vom tech4comp-Team wünschen dir viel Spaß und Erfolg mit dieser Analyse deiner im Text verfassten Vorstellungen.

In diesem Abschnitt wird das Wissensmodell aus deinem Text präsentiert. Die folgende Grafik (Abbildung 1) zeigt die Repräsentation deiner Ausarbeitung.

| -graph- |
|:--:|
| *Abbildung (1): Dein Modell |

Mittels einer computer-linguistischen Analysesoftware wurde die oben stehende Grafik auf der Grundlage deines Textes erstellt. Sie enthält die wichtigsten in deinem Text vorkommenden Vorstellungen. In deinem Text besonders stark assoziierte Begriffsverbindungen werden in roter Farbe dargestellt. Du erkennst dies auch daran, dass die Assoziationsstärke zwischen den Begriffen besonders hoch ist (das ist die kleine Zahl außerhalb der Klammer, und sie liegt zwischen 0 und 1). Immer noch wichtige aber weniger stark assoziierte Begriffsverbindungen werden in Blau dargestellt.
Achte bei der Einschätzung deines Modells auch auf das gesamte Netzwerk: Welche Begriffe sind ganz zentral und gehen viele Verbindungen mit benachbarten Konzepten ein und welche sind eher am Rande oder vielleicht sogar kaum mit dem Modell verbunden?


## Abschließende Bemerkung
Bei dem gesamten Analysevorgang zur Erstellung dieses Dokuments wurden keine personenbezogenen Daten von dir gespeichert. Auch deine Texte werden von den Systemen an der Martin-Luther-Universität Halle-Wittenberg wieder vollständig gelöscht.
Diese Auswertung ist Teil des vom BmBF geförderten Verbundprojektes tech4comp. Die Software für diese Auswertung wurde von Prof. Dr. Pablo Pirnay-Dummer für das Projekt entwickelt und steht Projektpartnern und kooperierenden Projekten zur Analyse zur Verfügung. Die Software hatte zum Zeitpunkt dieser Auswertung die Version 0.0.3.
Das Entwicklungsteam der Software hat möglicherweise keinen Einfluss darauf, in welchem didaktischen Rahmen dieser Software eingesetzt wird. Zur Anwendung der Software in deiner Lehrveranstaltung frage bitte deine Dozentinnen und Dozenten.