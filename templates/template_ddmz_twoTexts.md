# Dein Feedback zumm Vergleich zweier eigener Texte (themenunabhängig) mit TASA / T-TMITOCAR

## 1. Zwei Texte

Dieses Dokument enthält eine vergleichende Analyse zweier deiner Texte oder zweier Versionen eines Textes. Die Analyse wurde für dich automatisch und ausschließlich auf der Grundlage deiner Texte erstellt. Dabei wurden die Wissensstrukturen, die in deinen Texten verfasst sind, mittels der Software T-MITOCAR computer-linguistisch extrahiert, grafisch dargestellt, miteinander verglichen und mittels der Software TASA für dich interpretiert. Das Ergebnis dieser Analyse steht dir hier zur Verfügung. Wir vom tech4comp-Team wünschen dir viel Spaß und Erfolg mit dieser Analyse deiner im Text verfassten Vorstellungen.

In diesem Abschnitt wird das Wissensmodell aus deinem ersten Text präsentiert und mit dem Modell des zweiten Textes verglichen. Die folgende Grafik (Abbildung 1) zeigt die Repräsentation des ersten Textes:

| -graph1- |
|:--:|
| *Abbildung (1): Modell deines ersten Textes* |

Mittels einer computer-linguistischen Analysesoftware wurde die oben stehende Grafik auf der Grundlage deines Textes erstellt. Sie enthält die wichtigsten in deinem Text vorkommenden Vorstellungen. In deinem Text besonders stark assoziierte Begriffsverbindungen werden in roter Farbe dargestellt. Du erkennst dies auch daran, dass die Assoziationsstärke zwischen den Begriffen besonders hoch ist (das ist die kleine Zahl außerhalb der Klammer, und sie liegt zwischen 0 und 1). Immer noch wichtige aber weniger stark assoziierte Begriffsverbindungen werden in Blau dargestellt.
Achte bei der Einschätzung deines Modells auch auf das gesamte Netzwerk: Welche Begriffe sind ganz zentral und gehen viele Verbindungen mit benachbarten Konzepten ein und welche sind eher am Rande oder vielleicht sogar kaum mit dem Modell verbunden?

Die folgende Grafik (Abbildung 2) zeigt die Repräsentation deines zweiten Textes:

| -graph2- |
|:--:|
| *Abbildung (2): Modell deines zweiten Textes* |

Mittels einer computerlinguistischen Analysesoftware (T-MITOCAR) wurde die oben stehende Grafik auf der Grundlage deines zweiten Textes erstellt. Sie enthält die wichtigsten in diesem Text vorkommenden Vorstellungen.

## 2. Vergleich der Texte: Wissensmodelle

In diesem Abschnitt werden deine Texte verglichen. Dieser Vergleich berücksichtigt nicht nur die vorhandenen Begriffe, sondern analysiert das sogenannte ‘Wissensmodell’, die Verbindung zwischen den Begriffen. Bei Unterschieden in den Modellen geht es weniger um die vorhandenen Begriffe, sondern um die Art, wie diese Begriffe untereinander vernetzt sind. Dies kann im einfacheren Fall das Vorhandensein anderer Verknüpfungen betreffen, kann jedoch ebenso auf grundlegende Unterschiede im Wissensmodell hinweisen.

Schau dir in Ruhe die abweichenden Strukturen an. Kannst du daraus lernen, wie sich dein Text verändert hat? Welche Aussagen korrespondieren deiner Meinung nach mit den jeweiligen Strukturen?

Zwischen deinen beiden Texten ergeben sich somit einige Unterschiede in den Modellstrukturen. Diese Gemeinsamkeiten und Unterschiede werden im Folgenden zusammengefasst.

### 2.1 Gemeinsame Verbindungen

Die folgende Abbildung zeigt, welche Verbindungen in beiden deiner Texte vorkommen. 

| -graphIntersection- |
|:--:|
| *Abbildung 3: Gemeinsame Verbindungen* |

### 2.2 Hinzugekommene Verbindungen

Die folgende Abbildung zeigt, welche Verbindungen im zweiten Text vorhanden sind, jedoch nicht im ersten Text:

| -graphDiffB- |
|:--:|
| *Abbildung 4: Hinzugekommene Verbindungen* |

### 2.3 Nicht aufgenommene Verbindungen

Folgende Verbindungen sind im ersten Text vorhanden, finden sich jedoch nicht in deinem zweiten Text wieder:

| -graphDiffA- |
|:--:|
| *Abbildung 5: Entfallene Verbindungen* |

Damit kannst du die Entwicklung des Textes nachvollziehen.

## 3. Abschließende Bemerkung
Bei dem gesamten Analysevorgang zur Erstellung dieses Dokuments wurden keine personenbezogenen Daten von dir gespeichert. Auch deine Texte werden von den Systemen an der Martin-Luther-Universität Halle-Wittenberg wieder vollständig gelöscht.
Diese Auswertung ist Teil des vom BmBF geförderten Verbundprojektes tech4comp. Die Software für diese Auswertung wurde von Prof. Dr. Pablo Pirnay-Dummer für das Projekt entwickelt und steht Projektpartnern und kooperierenden Projekten zur Analyse zur Verfügung. Die Software hatte zum Zeitpunkt dieser Auswertung die Version 0.0.3.
Das Entwicklungsteam der Software hat möglicherweise keinen Einfluss darauf, in welchem didaktischen Rahmen dieser Software eingesetzt wird. Zur Anwendung der Software in deiner Lehrveranstaltung frage bitte deine Dozentinnen und Dozenten.
