# Dein Feedback zumm Vergleich zweier eigener Texte (themenunabhängig) mit TASA / T-TMITOCAR

## 1. Zwei Texte

Dieses Dokument enthält eine vergleichende Analyse zweier deiner Texte oder zweier Versionen eines Textes. Die Analyse wurde für dich automatisch und ausschließlich auf der Grundlage deiner Texte erstellt. Dabei wurden die Wissensstrukturen, die in deinen Texten verfasst sind, mittels der Software T-MITOCAR computer-linguistisch extrahiert, grafisch dargestellt, miteinander verglichen und mittels der Software TASA für dich interpretiert. Das Ergebnis dieser Analyse steht dir hier zur Verfügung. Wir vom tech4comp-Team wünschen dir viel Spaß und Erfolg mit dieser Analyse deiner im Text verfassten Vorstellungen.

In diesem Abschnitt wird das Wissensmodell aus deinem ersten Text präsentiert und mit dem Modell des zweiten Textes verglichen. Die folgende Grafik (Abbildung 1) zeigt die Repräsentation des ersten Textes:

| -graph1- |
|:--:|
| *Abbildung (1): Modell deines ersten Textes* |

Mittels einer computer-linguistischen Analysesoftware wurde die oben stehende Grafik auf der Grundlage deines Textes erstellt. Sie enthält die wichtigsten in deinem Text vorkommenden Vorstellungen. In deinem Text besonders stark assoziierte Begriffsverbindungen werden in roter Farbe dargestellt. Du erkennst dies auch daran, dass die Assoziationsstärke zwischen den Begriffen besonders hoch ist (das ist die kleine Zahl außerhalb der Klammer, und sie liegt zwischen 0 und 1). Immer noch wichtige aber weniger stark assoziierte Begriffsverbindungen werden in Blau dargestellt.
Achte bei der Einschätzung deines Modells auch auf das gesamte Netzwerk: Welche Begriffe sind ganz zentral und gehen viele Verbindungen mit benachbarten Konzepten ein und welche sind eher am Rande oder vielleicht sogar kaum mit dem Modell verbunden?

Die folgende Grafik (Abbildung 2) zeigt die Repräsentation deines zweiten Textes:

| -graph2- |
|:--:|
| *Abbildung (2): Modell deines zweiten Textes* |

Mittels einer computerlinguistischen Analysesoftware (T-MITOCAR) wurde die oben stehende Grafik auf der Grundlage deines zweiten Textes erstellt. Sie enthält die wichtigsten in diesem Text vorkommenden Vorstellungen.

## 2. Vergleich der Texte: Wissensmodelle

In diesem Abschnitt werden deine Texte verglichen. Natürlich kannst du das auch selbst tun, indem du die beiden vorher abgebildeten Netze betrachtest und nach Gemeinsamkeiten und Unterschieden suchst. Als Hilfestellung für diesen Vergleich bieten wir in diesem Abschnitt drei Wortlisten an.

Die erste Liste zeigt die Begriffe, die in beiden deiner Texte vorkommen. Damit kannst du auf einen Blick die ‚Schnittmenge' dieser beiden Texte sehen - zumindest was die Begriffe angeht.

| -BegriffeSchnittmenge- |
|:--:|
| *Abbildung 3: Liste der gemeinsamen Konzepte* |

Die zweite Liste sammelt die Begriffe, die du in deinem ersten Text verwendet hast, jedoch nicht im zweiten vorkommen. Mit Hilfe dieser Liste lässt sich gut sehen, auf welche Begriffe du dich im zweiten Text nicht mehr konzentriert hast.

| -BegriffeDiffA- |
|:--:|
| *Abbildung 4: Liste der weggefallenen Konzepte* |

Die dritte Liste umfasst die Begriffe, die in deinem zweiten Text neu hinzugekommen sind. Du kannst so die Entwicklung deiner Texte nachvollziehen.

| -BegriffeDiffB- |
|:--:|
| *Abbildung 5: Liste der hinzugekommenen Konzepte* |


## 3. Abschließende Bemerkung
Bei dem gesamten Analysevorgang zur Erstellung dieses Dokuments wurden keine personenbezogenen Daten von dir gespeichert. Auch deine Texte werden von den Systemen an der Martin-Luther-Universität Halle-Wittenberg wieder vollständig gelöscht.
Diese Auswertung ist Teil des vom BmBF geförderten Verbundprojektes tech4comp. Die Software für diese Auswertung wurde von Prof. Dr. Pablo Pirnay-Dummer für das Projekt entwickelt und steht Projektpartnern und kooperierenden Projekten zur Analyse zur Verfügung. Die Software hatte zum Zeitpunkt dieser Auswertung die Version 0.0.3.
Das Entwicklungsteam der Software hat möglicherweise keinen Einfluss darauf, in welchem didaktischen Rahmen dieser Software eingesetzt wird. Zur Anwendung der Software in deiner Lehrveranstaltung frage bitte deine Dozentinnen und Dozenten.
