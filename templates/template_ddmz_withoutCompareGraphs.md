# Dein Feedback zur Schreibaufgabe zum Thema -subject- in der VL Bildungstechnologien I (mit TASA / T-TMITOCAR)

## 1. Zwei Texte

Dieses Dokument enthält eine Analyse zu deiner Schreibaufgabe zum Thema -subject-. Die Analyse wurde für dich automatisch und ausschließlich auf der Grundlage der von dir bearbeiteten Schreibaufgabe erstellt. Dabei wurden die Wissensstrukturen, die in deinem Text verfasst sind, mittels der Software T-MITOCAR computer-linguistisch extrahiert, grafisch dargestellt, mit einem Mustertext verglichen und mittels der Software TASA für dich interpretiert. Das Ergebnis dieser Analyse steht dir hier zur Verfügung. Wir vom tech4comp-Team wünschen dir viel Spaß und Erfolg mit dieser Analyse deiner im Text verfassten Vorstellungen.

In diesem Abschnitt wird das Wissensmodell aus deinem Text präsentiert und mit dem Modell des Mustertextes verglichen. Die folgende Grafik (Abbildung 1) zeigt die Repräsentation deiner Ausarbeitung zum Thema -subject-:

| -graph2- |
|:--:|
| *Abbildung (1): Dein Modell zum Thema -subject-* |

Mittels einer computer-linguistischen Analysesoftware wurde die oben stehende Grafik auf der Grundlage deines Textes erstellt. Sie enthält die wichtigsten in deinem Text vorkommenden Vorstellungen zum Thema -subject-. In deinem Text besonders stark assoziierte Begriffsverbindungen werden in roter Farbe dargestellt. Du erkennst dies auch daran, dass die Assoziationsstärke zwischen den Begriffen besonders hoch ist (das ist die kleine Zahl außerhalb der Klammer, und sie liegt zwischen 0 und 1). Immer noch wichtige aber weniger stark assoziierte Begriffsverbindungen werden in Blau dargestellt.
Achte bei der Einschätzung deines Modells auch auf das gesamte Netzwerk: Welche Begriffe sind ganz zentral und gehen viele Verbindungen mit benachbarten Konzepten ein und welche sind eher am Rande oder vielleicht sogar kaum mit dem Modell verbunden?

Die folgende Grafik (Abbildung 2) zeigt die Repräsentation des Mustertextes zum Thema -subject-, das sogenannte Expertenmodell:

| -graph1- |
|:--:|
| *Abbildung (2): Das Expertenmodell zum Thema -subject-* |

Mittels einer computerlinguistischen Analysesoftware (T-MITOCAR) wurde die oben stehende Grafik auf der Grundlage des Mustertextes erstellt. Sie enthält die wichtigsten in diesem Text vorkommenden Vorstellungen zum Thema -subject-.

## 2. Vergleich der Modelle

In diesem Abschnitt wird deine Schreibaufgabe zum Thema -subject- mit dem entsprechenden Mustertext verglichen. Natürlich kannst du das auch selbst tun, indem du die beiden vorher abgebildeten Netze betrachtest und nach Gemeinsamkeiten und Unterschieden suchst. Als Hilfestellung für diesen Vergleich bieten wir in diesem Abschnitt drei Wortlisten an.

Die erste Liste zeigt die Begriffe, die sowohl in deinem Text als auch im Mustertext vorkommen. Damit kannst du auf einen Blick die ‚Schnittmenge' dieser beiden Texte sehen - zumindest was die Begriffe angeht. Gibt es darüber hinaus Begriffe, die dir für die Aufgabe wichtig erscheinen, die im Mustertext vielleicht sogar in der Nähe der Schnittmenge liegen, in deinem Text aber anders oder weniger aufgeführt werden?

| -BegriffeSchnittmenge- |
|:--:|
| *Abbildung 3: Liste der gemeinsamen Konzepte* |

Die zweite Liste sammelt die Begriffe, die du in deinem Text verwendet hast, jedoch nicht im Mustertext vorkommen. Mit Hilfe dieser Liste lässt sich gut sehen, wo du eigene Schwerpunkte gesetzt hast oder auch Themen mit einbezogen hast, die über den Fokus des Mustertextes hinausgehen. Hier kannst du überlegen, ob diese Ergänzungen gut zum Thema -subject- passen - oder vielleicht schon zu Themenschwerpunkten aus anderen Seminarsitzungen hinführen:

| -BegriffeDiffA- |
|:--:|
| *Abbildung 4: Liste deiner ‚eigenen' Konzepte* |

Die dritte Liste umfasst die Begriffe, die im Mustertext enthalten sind, aber bei dir fehlen. Überlege dir, warum die Begriffe in deinem Text nicht enthalten sind.

| -BegriffeDiffB- |
|:--:|
| *Abbildung 5: Liste der fehlenden Konzepte* |


## 3. Abschließende Bemerkung
Bei dem gesamten Analysevorgang zur Erstellung dieses Dokuments wurden keine personenbezogenen Daten von dir gespeichert. Auch deine Texte werden von den Systemen an der Martin-Luther-Universität Halle-Wittenberg wieder vollständig gelöscht.
Diese Auswertung ist Teil des vom BmBF geförderten Verbundprojektes tech4comp. Die Software für diese Auswertung wurde von Prof. Dr. Pablo Pirnay-Dummer für das Projekt entwickelt und steht Projektpartnern und kooperierenden Projekten zur Analyse zur Verfügung. Die Software hatte zum Zeitpunkt dieser Auswertung die Version 0.0.3.
Das Entwicklungsteam der Software hat möglicherweise keinen Einfluss darauf, in welchem didaktischen Rahmen dieser Software eingesetzt wird. Zur Anwendung der Software in deiner Lehrveranstaltung frage bitte deine Dozentinnen und Dozenten.
