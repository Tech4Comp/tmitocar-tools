# T-Mitocar-Tools

This repository contains CLI scripts to work with T-Mitocar and TASA APIs.

## Setup

1. Create the `auth.json` file by using `auth-example.json` as a template.
2. Install all dependencies listed below

Dependencies for tmitocar.sh:

* jq - parse json files - install via zypper if your distro won't provide the package
* docsplit - extract raw text from pdf files, optionally OCR and alignment correction
  * Use docsplit-ng instead (more recent)
* pandoc - conversion between different file formats

Dependencies for feedback.sh:

* pandoc - conversion between different file formats
* graphviz - create images from t-mitocar dot files
* wkhtmltopdf - pdf-engine used by pandoc

### docsplit specifics

Install via `sudo gem install docsplit-ng`

There might be an issue with the current ruby runtime, where "exists" was renamed for file existance checks to "exist" -> Head through all files of docsplit and simply replace these.

docsplit won't install its dependencies when the mentioned command is used. Install at least these tools/libs (by using your package manager) additionally:
* tesseract
* tesseract-langpack-deu
* tesseract-langpack-eng
* tesseract-osd

## Usage

Type `./<script_name>.sh -h` to see the usage information.

A typical call to `tmitocar.sh` looks like `./tmitocar.sh -i example.pdf`. There is also a batch-input system for T-Mitocar as `batchInput.sh`

In case a comparison of two models exists as a json file (created by `tmitocar.sh`), you can use `feedback.sh` to create custom feedback by using templates from the templates folder. A typical call looks like`./feedback.sh -i example.json -S "Test Subject" -t ./templates/template_ul.md`

## Processing Limitations

See the file [Recommendations & Todos](./Recommendations&Todos.md) to get an overview about the current limitations of the processing pipeline.

## Parsr - Transforms PDF, Documents and Images into Enriched Structured Data

Type `./parsr.sh -h` to see the usage information.

A typical call to this script looks like: `./parsr.sh -i example.pdf`

The script requires as a prerequisite the installation of the program Parsr. Under the following link you can find the installation instructions.

<https://github.com/axa-group/Parsr#installation>

Under certain circumstances it is possible that more memory must be allocated to the Parsr API. This is possible with the following command:

`docker run -p 3001:3001 -e NODE_OPTIONS=--max_old_space_size=8192 axarev/parsr`
