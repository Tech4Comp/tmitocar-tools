#!/bin/bash

# Remember to create the auth.json file. Use the file auth-example.json as a template.

printhelp () {
  echo "Usage: ./tmitocar.sh -i inputFile [-o outputFormat] [-l label] [-a authFile] [-L language] [-w wordspec] [-s] [-r]"
  echo "Usage: ./tmitocar.sh -S -i inputFile -l label [-a authFile] [-L language] [-w wordspec]"
  echo "This script currently supports reading txt, pdf and docx files"
  echo "Supported languages are en and de"
  echo "Possible output formats are: png, pdf, xls, csv, svg, dot, txt, json, graphml"
  echo "-s silentMode - do not wait for user input"
  echo "-S store model on server (defaults to 24h), use with -l"
  echo "-r advancedRepair - execute advanced text repair processes, which will take their time"
  echo
  echo "Usage: ./tmitocar.sh -l label1 -c label2 [-a authFile] [-T] [-o outputFormat]"
  echo "-T requests textual feedback of the comparison"
  echo "Possible output formats are: json, zip, pdf, txt"
}

if (($# == 0)); then
  echo "No arguments provieded!"
  printhelp
  exit 1;
fi

while getopts hsSrTi:o:l:a:L:c:w: options ; do
 case "${options}" in
 l) label=${OPTARG};;
 o) format=${OPTARG};;
 i)
    if [ -f "$OPTARG" ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified file does not exist!"
      exit 1 ;
    fi
    ;;
 a) authfile=${OPTARG} ;;
 L) language=${OPTARG};;
 c) label2=${OPTARG};;
 h) printhelp; exit ;;
 s) silentMode=true;;
 r) advancedRepair=true;;
 S) store=true;;
 T) textualFeedback=true;;
 w) # TODO text must be longer than wordspec, wordspec starts at 150
    if ! [[ "${OPTARG}" =~ ^[0-9]+$ ]] ; then
      echo "Argument wordspec requires a positive number (integer)"
      exit 1;
    else
      wordspec=${OPTARG}
    fi
    ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

silentMode=${silentMode:-false}
advancedRepair=${advancedRepair:-false}
store=${store:-false}
wordspec=${wordspec:-none}

if [ "$store" = true ] && [ -z $label ]; then
  echo "A label is needed when storing model server side. Please specify one by using the -l option."
  exit 1 ;
fi

if [ ! -z $label2 ] && [ -z $label ]; then
  echo "Two labels are required to compare models. Please specify both using the -l and -c options."
  exit 1 ;
fi

textualFeedback=${textualFeedback:-false}

if [ ! -z $label2 ] && [ ! -z $label ]; then
  if [ "$textualFeedback" = false ]; then
    format=${format:-txt}
  else
    format=${format:-zip}
  fi
else
  format=${format:-png}
fi

label=${label:-test}
language=${language:-de}
languageDocsplit=deu
languageHunspell="de_DE"
if [ "$language" = "en" ]; then
  languageDocsplit=eng
  languageHunspell="en_EN"
fi

if [ -z "$inputfile" ] && [ -z $label2 ] ; then
  echo "Option -i is required!"
  exit 1 ;
fi

if [ -z $authfile ] ; then
  echo "Trying to use default auth file: auth.json"
  authfile=auth.json
fi

if ! [ -f $authfile ] ; then
 echo "Auth file does not exist!"
 exit 1 ;
fi

date=$(date +'%Y%m%d')
authpkey=$(cat $authfile | jq -r '.pkey')
authukey=$(cat $authfile | jq -r '.ukey')
pkey=$(echo -n "$authpkey$date" | sha384sum | awk '{ print $1 }')

currentNanoSeconds=$(date +%s%N)
mkdir tmp-$currentNanoSeconds

if [ -z $label2 ]; then

  filetype=$(file -i "$inputfile" | cut -d' ' -f2)
  ending=".pdf"
  if [ "$filetype" = "text/plain;" ]; then
    ending=".txt"
    cp $inputfile tmp-$currentNanoSeconds
  elif [ "$filetype" = "application/pdf;" ]; then
    docsplit text $inputfile --language $languageDocsplit -o tmp-$currentNanoSeconds
    cat tmp-$currentNanoSeconds/*.txt > ${inputfile%$ending}.txt
  elif [ "$filetype" = "application/vnd.openxmlformats-officedocument.wordprocessingml.document;" ]; then
    ending=".docx"
    pandoc -s "$inputfile" -t plain -o "tmp-$currentNanoSeconds/${inputfile%$ending}.txt"
  else
    echo "Input filetype not supported!"
    exit 1;
  fi

  echo "Using chain for a "$ending" file"
  echo "Cleaning text ..."

  # clean text

  #textcleaned1=`cat tmp/*.txt | tr '\n' ':'`
  textcleaned=$(<tmp-"$currentNanoSeconds"/*.txt)
  textcleaned=$(echo "$textcleaned" | iconv -t UTF-8//TRANSLIT)
  textcleaned=$(echo "$textcleaned" | tr -dc '[:print:][:space:]äöüÄÖÜß' | iconv -ct UTF-8//IGNORE) # remove unwanted chars

  textcleaned=$(echo "$textcleaned" | sed -e 's/"//g' -e 's/“//g' -e 's/„//g' -e "s/'//g" -e "s/'//g" -e "s/‘//g" -e 's/\`//g') # quotes
  textcleaned=$(echo "$textcleaned" | sed -e 's/↑//g' -e 's/↓//g' -e 's/↕//g' -e "s/↨//g" -e "s/→//g" -e "s/←//g" -e "s/↔//g" -e "s/►//g" -e "s/◄//g" -e "s/▲//g" -e "s/▼//g") # arrows
  textcleaned=$(echo "$textcleaned" | sed -e 's/«//g' -e 's/»//g' -e 's/‹//g' -e "s/›//g" -e 's/&/und/g' -e 's/*//g' -e 's/~//g' -e 's/–//g')
  textcleaned=$(echo "$textcleaned" | sed -e 's/€//g' -e 's/¬//g' -e 's/™//g' -e 's/♪//g'  -e 's/♫//g' -e 's/§//g') # wide chars
  textcleaned=$(echo "$textcleaned" | sed -e 's/[\x01-\x1F\x7F]//g') # ASCII control chars
  textcleaned=$(echo "$textcleaned" | sed -e ':a;N;$!ba;s/-\n//g') # broken words
  textcleaned=$(echo "$textcleaned" | sed -e ':a;N;$!ba;s/\n/ /g' | sed -e 's/\.\s\{2,\}/. /g' -e 's/\s\{2,\}/. /g') # replace '\n' to ' ' and replace '.    ' to '. ' and replace '   ' to '. '
  textcleaned=$(echo "$textcleaned" | sed -e 's!http[s]\?://\S*!!g') # remove URLs
  textcleaned=$(echo "$textcleaned" | sed -e 's/[[:upper:]]\{2,\}/\L&/g') # uppercase sequences to lowercase

  if [ "$advancedRepair" = true ] ; then
    #try to repair separated words, like "Ha llo"
    IFS=' ' read -ra words <<< "$textcleaned"
    repaired=0

    for ((i = 0; i < ${#words[@]}; i++)); do # run over the whole text

      # search for words that do not exist in the language (use the current and the following word)
      hunspell_output=$(hunspell -d "$languageHunspell" -l <<< "${words[i]} ${words[i+1]}")

      if [ -n "$hunspell_output" ]; then # if there are any mistakes in those two words, try the concatenated variant
        direct_concatenation="${words[i]}${words[i+1]}"
        hunspell_output=$(hunspell -d "$languageHunspell" -l <<< "$direct_concatenation")
        if [ -z "$hunspell_output" ]; then # if the concatenated word exists in the language, replace the tuple in the text with the concatenated word
          # echo "${words[i]}" "${words[i+1]}"
          textcleaned=$(echo "$textcleaned" | sed -e "s/${words[i]} ${words[i+1]}/$direct_concatenation/g" || echo "$textcleaned")
          ((repaired++))
        fi
      fi
    done

    echo "Repaired $repaired separated words"
  fi;

  cleanedFile=${inputfile%$ending}\-cleaned.txt
  echo "$textcleaned" > $cleanedFile
  rm -rf tmp-$currentNanoSeconds
  echo "Text successfully cleaned and saved as "$cleanedFile
  echo ""
fi

#exit 2;

if [ -z $label2 ]; then
  if [ "$store" = false ]; then
    resultFile=${inputfile%$ending}\-modell.$format
    echo "Sending text to T-Mitocar and awaiting model..."
    curl -X POST -d "ukey=$authukey" -d "pkey=$pkey" -d "retrieval=receive" -d "lang=$language" -d "wordspec=$wordspec" -d "output=$format" -d "weighed=1" -d "discardafterseconds=1" -d "tmlabel=$label" --data-urlencode "tmtext@$cleanedFile" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tmitocar.pl > ${resultFile}
  else
    silentMode=true
    echo "Sending text to T-Mitocar and storing it there..."
    curl -X POST -L -d "ukey=$authukey" -d "pkey=$pkey" -d "retrieval=transmit" -d "lang=$language" -d "wordspec=$wordspec" -d "output=store" -d "weighed=1" -d "discardafterseconds=86400" -d "tmlabel=$label" --data-urlencode "tmtext@$cleanedFile" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tmitocar.pl
  fi
elif [ "$textualFeedback" = false ]; then
  case $format in
    'txt') ;;
    *) echo "Format ${format} not supported for this action. Use txt instead."; exit 1 ;;
  esac
  resultFile=comparison_${label}_vs_${label2}.${format}
  echo "Getting text comparison from T-Mitocar ..."
  curl -X POST -L -d "ukey=$authukey" -d "pkey=$pkey" -d "tmlabel1=$label" -d "tmlabel2=$label2" -d "output=list" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-compare.pl > ${resultFile}
else
  case $format in
    'zip') mluformat='pdfzip' ;;
    'pdf') mluformat='pdf' ;;
    'json') mluformat='json' ;;
    *) echo "Format ${format} not supported for this action. Use zip, pdf or json instead."; exit 1 ;;
  esac
  resultFile=comparison_${label}_vs_${label2}.${format}
  echo "Getting textual feedback of the text comparison from T-Mitocar ..."
  curl -X POST -L -d "ukey=$authukey" -d "pkey=$pkey" -d "tmlabel1=$label" -d "tmlabel2=$label2" -d "output=${mluformat}" -d "thema=test" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tasa.pl > ${resultFile}
fi

if [ ! -z ${resultFile} ]; then
  echo "Saved received file as "${resultFile}
else
  echo ""
fi

if [ "$silentMode" = false ] ; then
  read -n 1 -p "Do you want to open this file? [Y,n] " doit
  case $doit in
    y|Y|'') xdg-open ${resultFile} ;;
  esac
  echo ""
fi
