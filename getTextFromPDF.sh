#!/bin/bash

printhelp () {
  echo "Usage: ./getTextFromPDF.sh -i inputFile -l language"
  echo "This script currently supports reading pdf files"
  echo "Supported languages are en and de"
}

if (($# == 0)); then
  echo "No arguments provieded!"
  printhelp
  exit 1;
fi

while getopts hi:l: options ; do
 case "${options}" in
 i)
    if [ -f "$OPTARG" ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified file does not exist!"
      exit 1 ;
    fi
    ;;
 l) language=${OPTARG};;
 h) printhelp; exit ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

if [ -z "$inputfile" ] ; then
  echo "Option -i is required!"
  exit 1 ;
fi

language=${language:-de}
languageDocsplit=deu
if [ "$language" = "en" ]; then
  languageDocsplit=eng
fi

date=$(date +'%Y%m%d')

currentNanoSeconds=$(date +%s%N)
mkdir tmp-$currentNanoSeconds

filetype=$(file -i "$inputfile" | cut -d' ' -f2)
ending=".pdf"
if [ "$filetype" = "application/pdf;" ]; then
  docsplit text $inputfile --language $languageDocsplit -o tmp-$currentNanoSeconds
  cat tmp-$currentNanoSeconds/*.txt > ${inputfile%$ending}.txt
else
  echo "Input filetype not supported!"
  exit 1;
fi

echo "Repairing text ..."

textcleaned=$(<tmp-$currentNanoSeconds/*.txt)
textcleaned=`echo "$textcleaned" | iconv -c -t UTF-8`
textcleaned=`echo "$textcleaned" | sed -e ':a;N;$!ba;s/-\n//g'` # broken words

cleanedFile=${inputfile%$ending}\-cleaned.txt
echo "$textcleaned" > $cleanedFile
rm -rf tmp-$currentNanoSeconds
echo "Text successfully extracted and saved as "$cleanedFile
echo ""
