#!/bin/bash

printhelp () {
  echo "Usage: ./feedback.sh -i inputFile [-o outputFormat] [-t template] [-s]"
  echo "-i inputFile in .svg format"
  echo "Possible output formats are: pdf, odt, html"
  echo "-t template - specify a template file, which is used to create the output"
}

if (($# == 0)); then
  echo "No arguments provided!"
  printhelp
  exit 1;
fi

while getopts hst:i:o: options ; do
 case "${options}" in
 o) format=${OPTARG};;
 i)
    if [ -f "$OPTARG" ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified input file does not exist!"
      exit 1 ;
    fi
    ;;
 h) printhelp; exit ;;
 s) silentMode=true;;
 t)
    if [ -f "$OPTARG" ] ; then
      template=${OPTARG}
    else
      echo "Specified template file does not exist!"
      exit 1 ;
    fi
    ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

format=${format:-pdf}
if [ ! "$format" = "pdf" ] && [ ! "$format" = "odt" ] && [ ! "$format" = "html" ]; then
  echo "Output format not supported!"
  exit 1;
fi

template=${template:-'templates/template_ddmz_single.md'}
silentMode=${silentMode:-false}

filetype=$(file -i "$inputfile" | cut -d' ' -f2)
  ending=".svg"

resultFile=${inputfile%$ending}.$format

currentNanoSeconds=$(date +%s%N)
mkdir tmp-$currentNanoSeconds

echo "Creating pdf file..."

tmpTemplate=tmp-$currentNanoSeconds/template.md
cp $template $tmpTemplate

if [ "$format" = "pdf" ] || [ "$format" = "odt" ]; then
  path="tmp-$currentNanoSeconds\/"
else
  path=""
fi

if [ "$format" = "pdf" ] || [ "$format" = "html" ]; then
#  sed -i $tmpTemplate \
#  -e "s/-graph-/<img src=\"${inputfile}\" width=\"100%\" \/>/"
  perl -i -p -e 's/-graph-/<img src=\"'${inputfile//\//\\\/}'\" width=\"100%\" \/>/' $tmpTemplate
else
#  sed -i $tmpTemplate \
#  -e "s/-graph-/\!\[graph\](${inputfile})/"
  perl -i -p -e 's/-graph-/\!\[graph\]($ENV{inputfile})/' $tmpTemplate
fi

if [ "$format" = "html" ]; then
  resultFile=${resultFile}
fi

pandoc --pdf-engine wkhtmltopdf --metadata pagetitle="Textanalyse" -s -o $resultFile $tmpTemplate &> /dev/null

if [ "$format" = "html" ]; then
  zip -9 ${inputfile%$ending}.zip $inputfile $resultFile &> /dev/null
  resultFile=${inputfile%$ending}.zip
fi

rm -rf tmp-$currentNanoSeconds

if [ ! -z ${resultFile} ]; then
  echo "Saved file as "${resultFile}
else
  echo ""
fi

if [ "$silentMode" = false ] ; then
  read -n 1 -p "Do you want to open this file? [Y,n] " doit
  case $doit in
    y|Y|'') xdg-open ${resultFile} ;;
  esac
  echo ""
fi
