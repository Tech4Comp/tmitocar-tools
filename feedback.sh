#!/bin/bash

printhelp () {
  echo "Usage: ./feedback.sh -i inputFile [-o outputFormat] [-S subject] [-t template] [-s]"
  echo "This script currently supports reading json files"
  echo "Possible output formats are: pdf, odt, html"
  echo "-S subject - specify a subject/topic, used for the output"
  echo "-t template - specify a template file, which is used to create the output"
}

if (($# == 0)); then
  echo "No arguments provided!"
  printhelp
  exit 1;
fi

while getopts hsS:t:i:o: options ; do
 case "${options}" in
 o) format=${OPTARG};;
 i)
    if [ -f "$OPTARG" ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified input file does not exist!"
      exit 1 ;
    fi
    ;;
 h) printhelp; exit ;;
 S) subject=${OPTARG};;
 s) silentMode=true;;
 t)
    if [ -f "$OPTARG" ] ; then
      template=${OPTARG}
    else
      echo "Specified template file does not exist!"
      exit 1 ;
    fi
    ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

format=${format:-pdf}
if [ ! "$format" = "pdf" ] && [ ! "$format" = "odt" ] && [ ! "$format" = "html" ]; then
  echo "Output format not supported!"
  exit 1;
fi

subject=${subject:-'Example Subject'}
template=${template:-'templates/template_ul.md'}
silentMode=${silentMode:-false}

filetype=$(file -i "$inputfile" | cut -d' ' -f2)
  ending=".json"

resultFile=${inputfile%$ending}.$format

currentNanoSeconds=$(date +%s%N)
mkdir tmp-$currentNanoSeconds

echo "Extracting graphs..."

graph1=$(cat $inputfile | jq -r '.Graph1GraphViz')
graph2=$(cat $inputfile | jq -r '.Graph2GraphViz')

if [ "$graph1" == null ] || [ "$graph2" == null ] ; then
  echo "Not all graphs exist in input file, aborting ..."
  exit 1 ;
fi

echo "$graph1" > tmp-$currentNanoSeconds/graph1.dot
echo "$graph2" > tmp-$currentNanoSeconds/graph2.dot

graphDiffA=$(cat $inputfile | jq -r '.ModellDiffAGraphViz')
graphDiffB=$(cat $inputfile | jq -r '.ModellDiffBGraphViz')
graphIntersection=$(cat $inputfile | jq -r '.ModellSchnittGraphViz')

if [ "$graphDiffA"  != null ]; then
  echo "$graphDiffA" > tmp-$currentNanoSeconds/graphDiffA.dot
fi

if [ "$graphDiffB" != null ]; then
  echo "$graphDiffB" > tmp-$currentNanoSeconds/graphDiffB.dot
fi

if [ "$graphIntersection" != null ]; then
  echo "$graphIntersection" > tmp-$currentNanoSeconds/graphIntersection.dot
fi

begriffeSchnittmenge=$(cat $inputfile | jq -r '.BegriffeSchnittmenge')
begriffeDiffA=$(cat $inputfile | jq -r '.BegriffeDiffA')
begriffeDiffB=$(cat $inputfile | jq -r '.BegriffeDiffB')

echo "Creating graph images..."

for filename in tmp-$currentNanoSeconds/*; do
  ending2=${filename##*.}
  if [ $ending2 == "dot" ] ; then
    dot $filename -Tsvg -o ${filename%$ending2}svg &
  fi
done

wait

echo "Creating pdf file..."

tmpTemplate=tmp-$currentNanoSeconds/template.md
cp $template $tmpTemplate

if [ "$format" = "pdf" ] || [ "$format" = "odt" ]; then
  path="tmp-$currentNanoSeconds\/"
else
  path=""
fi

sed -i $tmpTemplate -e "s/-subject-/${subject}/"
if [ "$format" = "pdf" ] || [ "$format" = "html" ]; then
  sed -i $tmpTemplate \
  -e "s/-graph1-/<img src=\"${path}graph1.svg\" width=\"100%\" \/>/" \
  -e "s/-graph2-/<img src=\"${path}graph2.svg\" width=\"100%\" \/>/"
  if [ "$graphDiffA"  != null ] && [ "$graphDiffB" != null ] && [ "$graphIntersection" != null ] ; then
    sed -i $tmpTemplate \
    -e "s/-graphDiffA-/<img src=\"${path}graphDiffA.svg\" width=\"100%\" \/>/" \
    -e "s/-graphDiffB-/<img src=\"${path}graphDiffB.svg\" width=\"100%\" \/>/" \
    -e "s/-graphIntersection-/<img src=\"${path}graphIntersection.svg\" width=\"100%\" \/>/"
  fi
else
  sed -i $tmpTemplate \
  -e "s/-graph1-/\!\[graph1\](${path}graph1.svg)/" \
  -e "s/-graph2-/\!\[graph2\](${path}graph2.svg)/"
  if [ "$graphDiffA"  != null ] && [ "$graphDiffB" != null ] && [ "$graphIntersection" != null ] ; then
    sed -i $tmpTemplate \
    -e "s/-graphDiffA-/\!\[graphDiffA\](${path}graphDiffA.svg)/" \
    -e "s/-graphDiffB-/\!\[graphDiffB\](${path}graphDiffB.svg)/" \
    -e "s/-graphIntersection-/\!\[graphIntersection\](${path}graphIntersection.svg)/"
  fi
fi

if [ "$begriffeSchnittmenge"  != null ] && [ "$begriffeDiffA" != null ] ; then
  escaped1=${begriffeSchnittmenge//\"/}
  escaped11=${escaped1/\[/}
  escaped12=${escaped11/\]/}
  escaped2=${begriffeDiffA//\"/}
  escaped21=${escaped2/\[/}
  escaped22=${escaped21/\]/}
  sed -i $tmpTemplate -e "s/-BegriffeSchnittmenge-/\[${escaped12//$'\n'/}\]/" 
  sed -i $tmpTemplate -e "s/-BegriffeDiffA-/\[${escaped22//$'\n'/}\]/" 
fi

if [ "$begriffeDiffB" != null ] ; then
  escaped3=${begriffeDiffB//\"/}
  escaped31=${escaped3/\[/}
  escaped32=${escaped31/\]/}
  sed -i $tmpTemplate -e "s/-BegriffeDiffB-/\[${escaped32//$'\n'/}\]/"
fi

if [ "$format" = "html" ]; then
  resultFile=tmp-$currentNanoSeconds/${resultFile}
fi

pandoc --pdf-engine wkhtmltopdf --pdf-engine-opt=--enable-local-file-access --metadata pagetitle="${subject}" -s -o $resultFile $tmpTemplate &> /dev/null

if [ "$format" = "html" ]; then
  zip -9 ${inputfile%$ending}.zip tmp-$currentNanoSeconds/*.svg $resultFile &> /dev/null
  resultFile=${inputfile%$ending}.zip
fi

rm -rf tmp-$currentNanoSeconds

if [ ! -z ${resultFile} ]; then
  echo "Saved file as "${resultFile}
else
  echo ""
fi

if [ "$silentMode" = false ] ; then
  read -n 1 -p "Do you want to open this file? [Y,n] " doit
  case $doit in
    y|Y|'') xdg-open ${resultFile} ;;
  esac
  echo ""
fi
