## Das Mentoring-Konzept am Bonner Zentrum für Lehrerbildung

2.2 Grundideen

#### Als Unterstützung beim Bewältigen dieser Herausforderungen und Erreichen dieser Ziele können Stu-

#### dierende verschiedene Angebote des Mentorats nutzen (vgl. Abb. 2), die weiter unten näher beschrieben

#### werden. Das Grundprinzip ist dabei eine Kombination aus peer tutoring und peer mentoring-Elementen.

#### Peer tutoring beschreibt nach Topping (2005) einen Lehr-Lernprozess, in dem mindestens zwei Ler-

nende zunächst die Lehrer- und Schülerrolle festlegen und dann cokonstruktiv Inhalte oder Fähigkeiten
erarbeiten. Vorteile dieses Lehransatzes im Vergleich zu größeren Veranstaltungen sind die Möglichkeit

#### direkten Feedbacks (Greenwood u.a. 1990) und eine größere Anpassungsmöglichkeit der Instruktion an

#### individuelle Probleme der Lernenden (Moust & Schmidt 1994; De Rijdt u.a. 2012). Während das peer

#### tutoring' eher situationsgebundene Inhalte oder Fähigkeiten fokussiert, soll das peer mentoring auch

#### allgemeine Strategien vermitteln und vor allem psychosoziale Unterstützung liefern, z.B. in Form einer

#### Vorbildfunktion, und dadurch das Kompetenzempfinden der Lernenden verbessern (vgl. Kram 1983;

#### Terrion & Leonard 2007). Da das Mentorat sowohl bei der Vermittlung einer grundlegenden data lite-

#### racy (Z1) als auch bei der Begünstigung einer positiven Haltung gegenüber Forschung (Z2) unterstützen

#### soll (vgl. Abb. 2), werden beide Ansätze miteinander kombiniert.

2.3 Umsetzungsstrategien

#### Zur Umsetzung dieses Konzepts werden drei konkrete Strategien genutzt (vgl. Abb. 2): Ressourcenun-

#### terstützung, Workshops und Einzelberatung. Diese werden im Folgenden kurz vorgestellt und begrün-

#### det. Zunächst werden den Studierenden durch das Mentorat Ressourcen zur Verfügung gestellt, die in

#### der Regel nur in der universitären Forschung genutzt werden, sodass die Studierenden die Forscherrolle

#### besser einnehmen können (z.B. ein Hochleistungsscanner und Software zur Digitalisierung von ausge-

#### füllten Fragebögen, ein Arbeitsplatz mit Auswertungssoftware für quantitative und qualitative Daten

#### und ein Handapparat mit Literatur zu verschiedenen Forschungs- und Auswertungsmethoden). Diese

#### Ressourcen stehen den Studierenden zur Verfügung, um ihnen zur Entlastung ansonsten zeitaufwändige

#### Arbeiten zu verkürzen, wie das händische Eingeben von Daten (H3), um ambitioniertere Designs (z.B.

#### größere Stichproben) und Analysen zu ermöglichen, damit komplexere Fragestellungen bearbeitet wer-

#### den können (Z1), und um ihnen zu ermöglichen, eine authentischere Forscherrolle einzunehmen und

#### damit eine positivere Erfahrung mit Forschung machen zu können (Z2). Insgesamt soll dadurch eine

#### Verbindung zwischen der universitären Forschung und dem studentischen forschenden Lernen erstellt

#### werden. Darüber hinaus werden in Kooperation mit einzelnen Dozierenden der Vorbereitungs- und Be-

### gleitseminare zum Praxissemester Workshops zu verschiedenen Themen erarbeitet, um den Studieren-

#### den überblicksmäßig bestimmte Forschungsdesigns, Analysemethoden oder Datenvisualisierungen vor-

#### zustellen. In diesen Workshops können grundlegende Elemente der data literacy’ vermittelt werden

#### (H2), sodass die Studierenden zum Entwickeln von komplex(er)en Forschungsdesigns und Fragestel-

#### lungen angeregt werden (Z1). Darüber hinaus werden die Studierenden durch die Präsentation von Hin-

#### tergrundwissen angeregt, sich die Relevanz der Forschungsprojekte für die zukünftige Unterrichtspraxis

#### zu erschließen (Z2, Hl). Durch die enge Absprache mit den Dozierenden findet ein Austausch über

#### kontextspezifische Vorstellungen und Ansprüche an das forschende Lernen statt. Kernstück des Mento-

#### ring-Angebots stellt die Zinzelberatung der Studierenden durch einen Mentor dar, in der Fragen bezüg-

#### lich Forschungsdesign, Auswertungsmethoden, Visualisierungen und Interpretation der Daten behandelt

### werden, wodurch auf alle Teilbereiche der data literacy eingegangen werden kann. Gemeinsam erarbei-

#### tete Antworten werden stets in Form von Übungen mit scaffolding (vgl. Hmelo-Silver u.a. 2007) vertieft:

#### Die Studierenden führen zunächst einzelne Analysen gemeinsam mit dem Mentor durch, um ein grund-

#### legendes Verständnis für empirische Projekte im Lehramtsstudium für den Ablauf zu erhalten und wen-

#### den dann das Gelernte selbstständig an, wobei sie durch den Mentor direktes Feedback erhalten können.

```
! Dies ist eine Fußnote zum Testen der automatischen Textaufbereitung.
2 Das hier ist eine weitere Fußnote.
```

Dies ermöglicht es ihnen im Idealfall ihre restlichen Daten selbstständig zu analysieren. In der Einzel-

#### beratung stehen vor allem die Vermittlung von Kenntnissen zur Auswertung, Interpretation und Refle-

#### xion von Daten (Z1) sowie das Identifizieren und Ausgleichen von konkreten Wissenslücken (H2) im

#### Mittelpunkt. Damit nimmt das Mentorat gewissermaßen eine Brückenfunktion ein zwischen dem mo-

#### mentanen Leistungsstand der Studierenden und dem, was sie mit Unterstützung leisten könnten. Darüber

hinaus steht den Studierenden in der Einzelberatung ein Mentor als Ansprechpartner bei konkreten Prob-

#### lemen zur Verfügung, was im Sinne psychosozialer Unterstützung (vgl. Kram 1983; Terrion & Leonard

#### 2007) das Belastungsempfinden mindern könnte (H3). Zusammengefasst nehmen die Aktivitäten des

#### Mentorats Bezug auf alle Ziele und Herausforderungen des forschenden Lernens (siehe Abb. 2). Mög-

liche Limitationen und wahrgenommene Stärken dieses Ansatzes werden im folgenden Abschnitt prä-
sentiert.


