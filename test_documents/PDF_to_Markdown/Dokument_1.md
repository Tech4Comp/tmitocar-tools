```
1
```
# 1.1 ANDROGYNIE BEI KASTRATEN – FLUCH ODER SEGEN?

Das Schicksal der Kastraten im 17. und 18. Jahrhundert war sehr ambivalent. Die „engelgleichen Stimmen der
Kastraten“^1 wurden in der barocken Oper gefeiert und umjubelt, doch die Menschen hinter den Stimmen wurden
diskriminiert, ihre Männlichkeit wurde ihnen genommen. Massenhaft wurden Knabenverstümmelt, entmannt,
und schmerzhaften, teilweise tödlichen und offiziell streng verbotenen Operationen unterzogen, ohne dass die
Gewissheit bestand, dass sie als Sänger später Erfolg haben würden.^2

Kastraten übernahmen nicht nur, wie es aufgrund der hohen und weiblichen Stimme, aber auch bedingt durch
das Auftrittsverbot von Frauen in einem Kirchenstaatnahezuliegen scheint, alle weiblichen Rollen, sondern auch
die der Liebhaber, Feldherrn und Helden.^3 Eine androgyne Position wurde den Kastraten also nicht nur im ‚echten
Leben‘ durch ihre Kastration, sondern auch auf der Opernbühne, zuteil. Einige sahen darin einen enormen Vorteil
und einen „Beweis für den Vorrang der italienischen Musik“^4 : Kastraten hatten zwar eine weibliche, jedoch eine
sehr starke und kräftige Stimme; sie konnten jede Rolle, egal ob männlich oder weiblich, spielen, und hatten eine
enorme Übung darin, auch Frauenrollen überzeugenddarzustellen. Durch ihre Größe, die trotz Kastration der
eines ‚normalen‘ Mannes entsprach, hatten sie auch in der Rolle einer Frau eine als majestätisch beschriebene
Wirkung. Viele der von Kastraten verkörperten Frauen sollen von echten Frauen kaum oder gar nicht zu
unterscheiden gewesen sein; einigen Berichten zufolge waren Frauenrollen, die von Kastraten übernommen
wurden, sogar schöner.^5 In erster Linie sahen die Zuschauer der Barockoper also nicht das biologische Geschlecht
des Sängers, sondern die verkörperte Rolle.^6 Sie waren weniger das Gegenteil von Männlichkeit, sondern
besaßen „eine Flexibilität und eine über ein binäres Modell herausragende Vielfalt von Geschlechterrollen.“^7 Die
Kombination positiver Elemente von Maskulinität und Femininität bei Androgynen, wie beschrieben in Kapitel
2.1, trifft also –oberflächlich und nur auf professioneller Ebene– bei Kastraten zu.

Auf theologischer Ebene jedoch war der Kastraten Androgynie in keiner Weise ein Vorteil: Durch ihre fehlende
Potenz galt ein rechtliches Heiratsverbot, nach dem Tod rechneten sie mit Verdammnis – „die Kastraten
[standen] sowohl im Diesseits als auch im Jenseits im Abseits.“^8 Doch nicht nur aus theologischer Sicht wurde
Kastraten ihre Impotenz zum Verhängnis. Sie bedeutete auch, dass keine Familie gegründet werden konnte, also
keine Altersabsicherung existierte und sie im Alter mit Einsamkeit konfrontiert würden.^9 Affären wurden bei
hoch angesehenen und erfolgreichen Kastraten zwar geduldet, waren jedoch stets Gegenstand von Klatsch,

1
Bericht aus Filipp Balatris Memoiren, zit. nach: Ortkemper, Hubert: Engel wider Willen. Die Welt der Kastraten,
Henschel Verlag, Berlin 1993, S.7.

(^2) Vgl. Ebd.: S.19f.
(^3) Vgl. Walter, Michael: Gesang als höfische Rollen-Vernunft. Kastraten in der opera seria, in: Historische
Anthropologie 8, 2000, S.210.
(^4) Ortkemper: S.
(^5) Vgl. Ebd.
(^6) Vgl. Charton, Anke: prima donna, primo uomo, musico. Körper und Stimme: Geschlechterbilder in der Oper,
Leipziger Universitätsverlag, Leipzig 2011, S.198.
(^7) Ebd.
(^8) Ebd., S.199.
(^9) Vgl. Ebd.: S.200f.


```
2
```
Tratsch und Gerüchten. Ihnen wurde nachgesagt, nicht nur hetero-, sondern auch homosexuelle Beziehungen zu (^)
pflegen.^10 Kastraten litten außerhalb der Musikbranche unter sozialer Isolation; in einem Gedicht Schillers
erkennt dieser ihnen sogar die Fähigkeit ab, zu lieben: „Wer keinen Menschen machen kann, Der kann auch
keinen lieben.“^11 Zur zusätzlichen Erniedrigung der Kastraten existierte ein großes Repertoire an Schimpfworten:
Im Italienischen nannte man sie „caponne“ (Kapaun, ein gemästeter und kastrierter Hahn), „coglione“ (Hoden,
als Schimpfwort aber auch Arschloch) oder „evirato“ (der Entmannte)^12 , im Deutschen auch „Halbmänner“,
„Hämmling“ oder „Ohnegeil“^13. Jene Kastraten, die weder in der Oper, noch im Kirchenchor Erfolg hatten, litten
vermutlich am meisten unter ihrer Androgynie. Durch ihre Stimme wurden sie sofort als Kastraten identifiziert,
wodurch ihre Möglichkeiten auf zwei beschränkt waren: Einige wurden in den Priesterstand aufgenommen,^14
andere waren gezwungen, Bettler zu werden.^15 Durch ihre einzigartige und ambivalente Position –als Sänger
gefeiert, als Menschen missachtet und gedemütigt –konnten sich Kastraten weder der bürgerlichen, noch der
adeligen Welt zuordnen. Doch nicht nur bezüglich des sozialen Standes bestand Ungewissheit. Obwohl
Geschlechterunterschiede bis zur Mitte des 18. Jahrhunderts nicht anatomisch, sondern lediglich gesellschaftlich
und phänotypisch fundiert waren („one sex model“),^16 konnte die Geschlechterrolle der Kastraten nicht
klassifiziert werden. Sie waren weder Mann noch Frau und somit – trotz ihres Ruhmes - aufgrund ihrer
Androgynie nie richtig von der Gesellschaft akzeptiert. Die fortschreitende Aufklärung und die Abnahme
christlicher Askese bedeutete eine Hinwendung zu Natur und Natürlichkeit - die Androgynie der Kastraten wurde
als unnatürlich, als künstlich angesehen. So ging auch nach und nach die Glanzzeit der Kastraten zu Ende.^17
(^10) Rosselli, John: The Castrati as a Professional Group and a Social Phenomenon. 1550 - 1850, in: Acta
Musicologica, Vol. 60, 1988, S.176. 11
Schiller, Friedrich: Kastraten und Männer. 1782, V.110f.
(^12) Vgl. Rosselli: S.174.
(^13) Vgl. Ortkemper: S.267.
(^14) Vgl. Charton: S.201.
(^15) Vgl. Ortkemper: S.259.
(^16) Vgl. Laqueur, Thomas: Making Sex. Body and Gender from the Greeks to Freud, 10. Aufl., Harvard University
Press, Cambridge u.a. 1990, S.149.
(^17) Vgl. Rosselli: S.179.


