### Ein Mentoring-Ansatz für empirische Projekte im

```
Lehramtsstudium:
```
## Möglichkeiten zur Unterstützung

## bei der Durchführung von empirischen Studien in

## Praxisphasen des Studiums

```
{abraham, dieter, frauke} (@abc.org
```
###### Abstract

###### Praxisphasen im Lehramtsstudium haben in den letzten Jahren stetig an Bedeutung gewonnen, um eine engere

###### Verknüpfung von Theorie und Praxis zu ermöglichen. Diese engere Verknüpfung kann sich zum Beispiel darin äußern,

###### dass (angehende) Lehrkräfte Forschungsmethoden zur Analyse, Reflexion (im Sinne einer kritischen Hinterfragung) und

###### Verbesserung des eigenen Unterrichts anwenden und in der Lage sind, aktuelle Lehr- und Lernforschung zur

###### kontinuierlichen Erweiterung ihres Professionswissens zu rezipieren.

```
Keywords: Mentor, Tutoring
```
```
1.1 Grundideen
```
###### Als Unterstützung beim Bewältigen dieser

###### Herausforderungen und Erreichen dieser Ziele können

###### Studierende verschiedene Angebote des Mentorats nutzen

```
(vgl. Abb. 2), die weiter unten näher beschrieben werden.
```
###### Das Grundprinzip ist dabei eine Kombination aus peer

###### tutoring und peer mentoring-Elementen. Peer tutoring

###### beschreibt nach Topping (2005) einen Lehr-Lernprozess, in

###### dem mindestens zwei Lernende zunächst die Lehrer- und

```
Schülerrolle festlegen und dann cokonstruktiv Inhalte oder
Fähigkeiten erarbeiten. Vorteile dieses Lehransatzes im
```
###### Vergleich zu größeren Veranstaltungen sind die

###### Möglichkeit direkten Feedbacks (Greenwood u.a. 1990)

###### und eine größere Anpassungsmöglichkeit der Instruktion

###### an individuelle Probleme der Lernenden (Moust & Schmidt

###### 1994; De Rijdt u.a. 2012). Während das peer tutoring' eher

###### situationsgebundene Inhalte oder Fähigkeiten fokussiert,

###### soll das peer mentoring auch allgemeine Strategien

```
vermitteln und vor allem psychosoziale Unterstützung
```
###### liefern, z.B. in Form einer Vorbildfunktion, und dadurch

###### das Kompetenzempfinden der Lernenden verbessern (vgl.

###### Kram 1983; Terrion & Leonard 2007). Da das Mentorat

###### sowohl bei der Vermittlung einer grundlegenden data

###### literacy (Z1) als auch bei der Begünstigung einer positiven

###### Haltung gegenüber Forschung (Z2) unterstützen soll (vgl.

###### Abb. 2), werden beide Ansätze miteinander kombiniert.

#### 2. Umsetzungsstrategien

###### Zur Umsetzung dieses Konzepts werden drei konkrete

###### Strategien genutzt (vgl. Abb. 2): Ressourcenunterstützung,

##### Workshops und Einzelberatung. Diese werden im

###### Folgenden kurz vorgestellt und begründet. Zunächst

###### werden den Studierenden durch das Mentorat Ressourcen

###### zur Verfügung gestellt, die in der Regel nur in der

###### universitären Forschung genutzt werden, sodass die

###### Studierenden die Forscherrolle besser einnehmen können

###### (z.B. ein Hochleistungsscanner und Software zur

###### Digitalisierung von ausgefüllten Fragebögen, ein

###### Arbeitsplatz mit Auswertungssoftware für quantitative und

```
! Dies ist eine Fußnote zum Testen der automatischen Textauf-
bereitung.
```
###### qualitative Daten und ein Handapparat mit Literatur zu

###### verschiedenen Forschungs- und Auswertungsmethoden).

###### Diese Ressourcen stehen den Studierenden zur Verfügung,

###### um ihnen zur Entlastung ansonsten zeitaufwändige

###### Arbeiten zu verkürzen, wie das händische Eingeben von

```
Daten (H3), um ambitioniertere Designs (z.B. größere
```
###### Stichproben) und Analysen zu ermöglichen, damit

###### komplexere Fragestellungen bearbeitet werden können

###### (ZI), und um ihnen zu ermöglichen, eine authentischere

###### Forscherrolle einzunehmen und damit eine positivere

###### Erfahrung mit Forschung machen zu können (Z2).

###### Insgesamt soll dadurch eine Verbindung zwischen der

###### universitären Forschung und dem _ studentischen

###### forschenden Lernen erstellt werden. Darüber hinaus

###### werden in Kooperation mit einzelnen Dozierenden der

###### Vorbereitungs- und Begleitseminare zum Praxissemester

##### Workshops zu verschiedenen Themen erarbeitet, um den

###### Studierenden überblicksmäßig bestimmte

##### Forschungsdesigns, Analysemethoden oder

###### Datenvisualisierungen vorzustellen. In diesen Workshops

###### können grundlegende Elemente der data _literacy?

###### vermittelt werden (H2), sodass die Studierenden zum

###### Entwickeln von komplex(er)en Forschungsdesigns und

###### Fragestellungen angeregt werden (Z1). Darüber hinaus

###### werden die Studierenden durch die Präsentation von

###### Hintergrundwissen angeregt, sich die Relevanz der

###### Forschungsprojekte für die zukünftige Unterrichtspraxis zu

###### erschließen (Z2, Hl). Durch die enge Absprache mit den

###### Dozierenden findet ein Austausch über kontextspezifische

###### Vorstellungen und Ansprüche an das forschende Lernen

###### statt. Kernstück des Mentoring-Angebots stellt die

###### Einzelberatung der Studierenden durch einen Mentor dar,

###### in der Fragen bezüglich Forschungsdesign,

##### Auswertungsmethoden, Visualisierungen und

```
Interpretation der Daten behandelt werden, wodurch auf
```
###### alle Teilbereiche der data literacy eingegangen werden

##### kann. Gemeinsam erarbeitete Antworten werden stets in

###### Form von Übungen mit scaffolding (vgl. Hmelo-Silver u.a.

```
2007) vertieft: Die Studierenden führen zunächst einzelne
```
###### Analysen gemeinsam mit dem Mentor durch, um ein

```
? Das hier ist eine weitere Fußnote.
```

###### grundlegendes Verständnis für empirische Projekte im

###### Lehramtsstudium für den Ablauf zu erhalten und wenden

dann das Gelernte selbstständig an, wobei sie durch den

###### Mentor direktes Feedback erhalten können. Dies

ermöglicht es ihnen im Idealfall ihre restlichen Daten

###### selbstständig zu analysieren. In der Einzelberatung stehen

###### vor allem die Vermittlung von Kenntnissen zur

Auswertung, Interpretation und Reflexion von Daten (Z1)

###### sowie das Identifizieren und Ausgleichen von konkreten

###### Wissenslücken (H2) im Mittelpunkt. Damit nimmt das

###### Mentorat gewissermaßen eine Brückenfunktion ein

##### zwischn dem momentanen Leistungsstand der

###### Studierenden und dem, was sie mit Unterstützung leisten

###### könnten. Darüber hinaus steht den Studierenden in der

###### Einzelberatung ein Mentor als Ansprechpartner bei

###### konkreten Problemen zur Verfügung, was im Sinne

###### psychosozialer Unterstützung (vgl. Kram 1983; Terrion &

###### Leonard 2007) das Belastungsempfinden mindern könnte

(H3). Zusammengefasst nehmen die Aktivitäten des

###### Mentorats Bezug auf alle Ziele und Herausforderungen des

###### forschenden Lernens (siehe Abb. 2). Mögliche

Limitationen und wahrgenommene Stärken dieses

###### Ansatzes werden im folgenden Abschnitt präsentiert.

##### 3. Bibliographie

Castor, A. and Pollux, L. E. (1992). The use of user

###### modelling to guide inference and learning. Applied

```
Intelligence, 2(1):37-53.
```
###### Grandchercheur, L.B. (1983). Vers une modelisation

```
cognitive de l'ötre et du n&ant. In S.G Paris, G.M. Olson,
```
###### & H.W. Stevenson (Eds.), Fondement des Sciences

###### Cognitives. Hillsdale, NJ: Lawrence Erlbaum

```
Associates, pp. 6--38.
```
###### Strötgen, J. and Gertz, M. (2012). Temporal tagging on

```
different domains: Challenges, strategies, and gold
```
###### standards. In Nicoletta Calzolari (Conference Chair), et

###### al., editors, Proceedings of the Eight International

###### Conference on Language Resources and Evaluation

###### (LREC’12), pages 3746-3753, Istanbul, Turkey, may.

###### European Language Resource Association (ELRA).

###### Superman, S., Batman, B., Catwoman, C., and Spiderman,

###### S. (2000). Superheroes experiences with books. The

##### Phantom Editors Associates, Gotham City, 20th edition.


